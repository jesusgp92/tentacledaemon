import datetime
import dropbox
import json
import logging
import os
import sys
import time

from apscheduler.executors.pool import ThreadPoolExecutor
from apscheduler.jobstores.sqlalchemy import SQLAlchemyJobStore
from apscheduler.schedulers import SchedulerNotRunningError
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.date import DateTrigger
from apscheduler.triggers.interval import IntervalTrigger

from daemon import Daemon
from settings import ACCESS_TOKEN, MONITOR_UNIQUE_IDENTIFIER, DATA_FOLDER
from config import set_link
from utils import get_file_type
from uploading import upload_traces


PIDFILE = "monitor.pid"

logging.getLogger()

class TentacleDaemon(Daemon):
    
    def init_scheduler(self):
        executors = {'default': ThreadPoolExecutor(10)}
        jobstores = {'default': SQLAlchemyJobStore(url='sqlite:///jobs.sqlite')}
        self.scheduler = BackgroundScheduler(executors = executors,jobstores=jobstores)
        
    def init_jobs(self):
        now = datetime.datetime.now()
        next_hour = datetime.datetime(year=now.year,
                                      month=now.month,
                                      day=now.day,
                                      hour=now.hour)
        next_hour += datetime.timedelta(hours=1)
        trigger = IntervalTrigger(hours=1,start_date=next_hour)
        self.scheduler.add_job(func=upload_traces,
                               trigger=trigger,
                               id="0",
                               name="upload traces",
                               replace_existing=True)
    
    def __init__(self, pidfile,daemonize, stdin='/dev/null', stdout='/dev/null', stderr='/dev/null'):
        Daemon.__init__(self, pidfile,daemonize, stdin=stdin, stdout=stdout, stderr=stderr)
        self.init_scheduler()
        
    def remove_file(self,path,client):
        try:
            client.file_delete(path)
        except dropbox.rest.ErrorResponse as e:
            logging.error("it was not possible to delete the manifest file")
    
    def handle_test_file(self,data,path,client):
                
        if "id" in data:
            plan_id = str(data["id"])
        else:
            logging.error("Missing id parameter, skipping file %s" % path)
            return
        
        job = self.scheduler.get_job(job_id=plan_id)
        
        if "disable" in data:
            if data["disable"] and job != None:
                job.remove()
                self.remove_file(path, client)
                return

        if "kwargs" in data:
            kwargs=data["kwargs"]
        else:
            logging.error("Missing kwargs parameter, skipping file %s" % path)
            return
        
        if "name" in data: 
            name = data["name"]
            func= "monitor.plugins." + str(name) + ".__init__:run"
        else:
            logging.error("Missing name parameter, skipping file %s" % path)
            return
        
        trigger = None
        misfire_grace_time = None
        if "interval" in data:
            if data["interval"] == None:
                start_date = datetime.datetime.strptime(data["start_date"],"%Y-%m-%d %H:%M:%S")
                trigger = DateTrigger(run_date=start_date)
                misfire_grace_time = 300
            else:
                start_date = None
                if "start_date" in data:
                    if not data["start_date"] == None:
                        start_date = datetime.datetime.strptime(data["start_date"],"%Y-%m-%d %H:%M:%S")
                end_date = None 
                if "end_date" in data:
                    if not data["end_date"] == None:
                        end_date = datetime.datetime.strptime(data["end_date"],"%Y-%m-%d %H:%M:%S")
                trigger = IntervalTrigger(seconds=data["interval"],start_date=start_date, end_date=end_date)
        else:
            logging.error("Missing interval parameter, skipping file %s" % path)
            return
        
        if job == None:
            self.scheduler.add_job(func=func,
                                   trigger=trigger,
                                   kwargs=kwargs,
                                   id=plan_id,
                                   name=name,
                                   coalescing = True,
                                   misfire_grace_time=misfire_grace_time,
                                   replace_existing=True)
        else:
            job.modify(kwargs=kwargs)
            job.reschedule(trigger=trigger)
        
        self.remove_file(path, client)

        if not os.path.exists(os.path.join(DATA_FOLDER,name)):
            os.mkdir(os.path.join(DATA_FOLDER,name))
            
    def handle_link_file(self,data,path,client):
        
        link = {}

        if "ip" in data and "status" in data and "id" in data:
            link["ip"]=data["ip"]
            link["status"]=data["status"]
            link["id"]=data["id"]
        else:
            logging.error("malformed link file, skipping file %s"%path)
        
        set_link(link)
        self.remove_file(path, client)
        
        
    def check_file(self,path,client):
        try:
            f = client.get_file(path)
        except dropbox.rest.ErrorResponse as e:
            logging.error("It was not possible to retrieve the file")
            return
        
        try:
            data = json.loads(f.read())
        except Exception as e:
            logging.error("Couldn't parse file %s, skipping, exception: %s"%(path,e))
            return
        
        file_type = get_file_type(path)
        if file_type == "link":
            self.handle_link_file(data,path,client)
        elif file_type == "test":
            self.handle_test_file(data,path,client)
        else:
            logging.error("Unknown file type, skipping file %s"%path)
        
                
    def poll_dropbox(self):
        
        client = dropbox.client.DropboxClient(ACCESS_TOKEN)
        cursor = None
        
        while True:
            result = client.delta(cursor,path_prefix='/monitor%s/plans'%MONITOR_UNIQUE_IDENTIFIER)
            
            cursor = result['cursor']
            for path, metadata in result['entries']:
                if metadata is not None and not metadata["is_dir"]:
                    #if a file was added/changed we need to edit those changes
                    self.check_file(path,client)
                else:
                    pass #file deletion is skipped
            
            # if has_more is true, call delta again immediately
            if not result['has_more']:
                changes = False
                
            # poll until there are changes
            while not changes:
                try:
                    response = client.longpoll_delta(cursor)
                except Exception as e:
                    logging.error(e)
                    time.sleep(5)
                    response = {"changes":False,"backoff":None}
                    
                changes = response['changes']
                if not changes:
                    backoff = response.get('backoff', None)
                    if backoff is not None:
                        time.sleep(backoff)
                        
    def print_jobs(self):
        self.scheduler.print_jobs(out=sys.stdout)
                
    def run(self):
        self.init_jobs()
        self.scheduler.start()
        if not os.path.exists(DATA_FOLDER):
            os.mkdir(DATA_FOLDER)
            logging.info("created the staging area directory")
        logging.info("Tentacle Daemon started")
        self.poll_dropbox()
    
    def stop(self):
        try:
            self.scheduler.shutdown()
        except SchedulerNotRunningError:
            pass
        logging.info("Tentacle Daemon shut down")
        Daemon.stop(self)
