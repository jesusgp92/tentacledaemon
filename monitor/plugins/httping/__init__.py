import logging
import os
import requests
from requests.exceptions import RequestException
from subprocess import check_output, CalledProcessError
from threading import Thread
import time

from monitor.config import get_config
from monitor.settings import DATA_FOLDER


log = logging.getLogger()

def run(**kwargs):
    """makes an http header GET request to get status code and RTT
       parameters (per link):
        port: http port
        path: path to GET
    """

    config = get_config()
    if config is None:
        return
    
    
    links = config["links"]
    timeout = kwargs["timeout"]
      
    for link in links.values():
        if not link["status"]:
            continue
        filename = os.path.join(DATA_FOLDER,"httping","link_%s_%s.txt"%(link["id"],str(time.strftime("%d_%m_%y_%H"))))
        output_file = open(filename,'a')
        current_time = time.time()
        try:
            url = "http://"+link["ip"]
            port_name = "port_%s"%link["id"]
            path_name = "path_%s"%link["id"]
            
            if port_name in kwargs:
                url += ":" + str(kwargs[port_name])
            if path_name in kwargs:
                if kwargs[path_name]:
                    url += kwargs[path_name]      
            response = requests.head(url,timeout=timeout)
            output_file.write("%s %s %s\n"%(current_time,response.elapsed,response.status_code))
        except RequestException as e:
            output_file.write("%s %s %s\n"%(current_time,-1,-1))
        output_file.close()
    
    