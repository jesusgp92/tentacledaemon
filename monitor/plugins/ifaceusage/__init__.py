import logging
import os
from subprocess import Popen, PIPE
from threading import Thread
from time import sleep
import time


DATA_FOLDER = '/var/tmp/octopus'

def parse_bmon_output(output,path):
    if output[0] == 'I':
        return
    filename = os.path.join(path,"trace_%s.txt"%str(time.strftime("%d_%m_%y_%H")))
    with open(filename,'a') as f:
        output = output.strip()
        tokens = output.split()
        trace = tokens[1]+","+tokens[2]+","+tokens[3]+","+tokens[4]+'\n'
        print "writing a trace..."
        f.write(trace)

def run(iface='eth0',sample_interval=1,time=90):
    path = os.path.join(DATA_FOLDER,"ifaceusage")
    if not os.path.exists(path):
        os.mkdir(path)
    try:
        number = 3600
        p = Popen(["bmon","-o","ascii:quitafter=%s"%number,"-p",iface,"-r",str(sample_interval)],
                   stdout=PIPE, bufsize=1)
        with p.stdout:
            for line in iter(p.stdout.readline, b''):
                parse_bmon_output(line, path)
        p.wait()

    except Exception as e:
        logging.error(e)
        
run()