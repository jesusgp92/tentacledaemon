import logging
import os
from subprocess import check_output, CalledProcessError
from threading import Thread
import time

from monitor.config import get_config
from monitor.settings import DATA_FOLDER, OS


log = logging.getLogger()

def parse_ping_output(out,link_id):
    filename = os.path.join(DATA_FOLDER,"ping","link_%s_%s.txt"%(link_id,str(time.strftime("%d_%m_%y_%H"))))
    output_file = open(filename,'a')
    
    lines= out.split('\n')
      
    for line in lines:
        if line.startswith('['):
                tokens = line.split()
                timestamp = tokens[0].replace("[","").replace("]","")
                icmp = ""
                rtt = ""
                for token in tokens:
                    if "icmp_seq=" in token:
                        icmp = token.replace("icmp_seq=","")
                    elif "icmp_req" in token:
                        icmp = token.replace("icmp_req=","")
                    elif "time=" in token:
                        rtt=token.replace("time=","")
                output_file.write(timestamp+" "+icmp+" "+rtt+"\n")
    output_file.close()
    
def parse_ping_osx(out,link_id,start_time,interval):
    filename = os.path.join(DATA_FOLDER,"ping","link_%s_%s.txt"%(link_id,str(time.strftime("%d_%m_%y_%H"))))
    output_file = open(filename,'a')
    
    lines= out.split('\n')
    
    i = 0
    for line in lines:
        if "time=" in line:
                tokens = line.split()
                timestamp = start_time + i*interval
                i = i + 1
                icmp = ""
                rtt = ""
                for token in tokens:
                    if "icmp_seq=" in token:
                        icmp = token.replace("icmp_seq=","")
                    elif "icmp_req" in token:
                        icmp = token.replace("icmp_req=","")
                    elif "time=" in token:
                        rtt=token.replace("time=","")
                output_file.write(str(timestamp)+" "+icmp+" "+rtt+"\n")
    output_file.close()
    
def ping_test(link,count,interval,W):
    test_start_time = time.time()
    try:
        if OS == "linux":
            out = check_output(["ping",link["ip"],"-D","-c",str(count),"-i",str(interval),"-W",str(W)])
            parse_ping_output(out,link["id"])
        elif OS == "OS X":
            start_time = time.time()
            out = check_output(["ping","-c",str(count),"-i",str(interval),"-t",str(W),link["ip"]])
            parse_ping_osx(out,link["id"],start_time,interval)
    except Exception as e:
        log.error(str(e))
        out = "%s %s %s\n" %(str(test_start_time),"1","-1")
        filename = os.path.join(DATA_FOLDER,"ping","link_%s_%s.txt"%(link["id"],str(time.strftime("%d_%m_%y_%H"))))
        output_file = open(filename,'a')
        output_file.write(out)
        output_file.close()

def run(count=10,timeout=10,interval=1):
    """executes ping command for each of the test links specified in the config file
    options: -D shows timespam
             -c number of icmp packages to send
             -i interval between each package
             -W timeout
    """
    
    config = get_config()
    if config is None:
        return
    
    links = config["links"]
    
    threads = []
    for link in links.values():
        if not link["status"]:
            continue
        thread = Thread(target = ping_test, args = (link,count,interval,timeout))
        threads.append(thread)
        thread.start()
    for thread in threads:
        thread.join()
