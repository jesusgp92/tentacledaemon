import json
import logging
import os
from subprocess import check_output, CalledProcessError
import sys
from threading import Thread
import time

from monitor.config import get_config
from monitor.settings import DATA_FOLDER




log = logging.getLogger()

def parse_iperf_output(out,link_id,is_reverse):
    path = os.path.join(DATA_FOLDER,"iperf")
    if not os.path.exists(path):
        os.mkdir(path)
    filename = os.path.join(path,"link_%s_%s.txt"%(link_id,str(time.strftime("%d_%m_%y_%H"))))
    output_file = open(filename,'a')
    
    lines = out.split('\n')
    print out
    line = lines[-1]
    if line == "":
        line = lines[-2]
    
    tokens = line.split(',')
    timestamp,bytes_trasnfered,bits_per_second = tokens[0],tokens[7],tokens[8]
    
    result = timestamp+","+is_reverse+","+bytes_trasnfered+","+bits_per_second+'\n'
    
    output_file.write(result)
    output_file.close()
    
def iperf_test(link,reverse,_time,_bytes,flows,port):
    try:
        args = ["iperf","-c",link["ip"],"-y","C","-p",str(port)]
        if _time != None:
            args.append("-t")
            args.append(str(_time))
        elif _bytes != None:
            args.append("-n")
            args.append(_bytes)
        if flows != None:
            args.append("-P")
            args.append(str(flows))
        out = check_output(args)
        parse_iperf_output(out,link["id"],"1")
    except Exception as e:
        logging.error(e)
    if not reverse:
        return
    try:
        args.append("-R")
        out = check_output(args)
        parse_iperf_output(out,link["id"],"0")
    except Exception as e:
        logging.error(e)

def run(**kwargs):
    """executes iperf command for each of the test links specified in the config file
    options: 
    """
    
    config = get_config()
    if config is None:
        return
    
    links = config["links"]
    reverse = kwargs.get("test bidirectional speed",True)
    for link in links.values():
        test_this_link = kwargs.get("test this link_%s"%link["id"],False)
        if test_this_link:
            _time = kwargs.get("time to transmit_%s"%link["id"],None)
            _bytes = kwargs.get("bytes to send_%s"%link["id"],None)
            flows = kwargs.get("number of flows_%s"%link["id"],1)
            port = kwargs.get("port_%s"%link["id"],5001)
            iperf_test(link,reverse,_time,_bytes,flows,port)