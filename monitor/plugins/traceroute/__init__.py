import json
import logging
import os
from subprocess import check_output, CalledProcessError
from threading import Thread
import time

from monitor.config import get_config
from monitor.plugins.traceroute.tracerouteparser import TracerouteParser
from monitor.settings import DATA_FOLDER, OS


log = logging.getLogger()

def parse_traceroute_output(out,link_id,current_time):
    filename = os.path.join(DATA_FOLDER,"traceroute","link_%s_%s.txt"%(link_id,str(time.strftime("%d_%m_%y_%H"))))
    output_file = open(filename,'a')
    
    trp = TracerouteParser()
    trp.parse_data(out)
    
    hops = []
    for hop in trp.hops:
        probes = []
        for probe in hop.probes:
            probes.append(probe.__dict__)
        hops.append(probes)
    json_string = json.dumps(hops)
    output_file.write(str(current_time) + ' ' + json_string+'\n')
    output_file.close()
    
def traceroute_test(link,m,n):
    
    try:
        current_time = time.time()
        if OS == "linux":
            out = check_output(["traceroute",link["ip"],"-m",str(m),"-n",str(n)])
            parse_traceroute_output(out,link["id"],current_time)
        elif OS == "OS X":
            out = check_output(["traceroute","-m",str(m),"-q",str(n),link["ip"]])
            parse_traceroute_output(out,link["id"],current_time)
    except CalledProcessError as e:
        log.error("traceroute error: %s" % e)

def run(**kwargs):
    """executes traceroute command for each of the test links specified in the config file
    options: -m maximum ttl -n number of queries
    """
    config = get_config()
    if config is None:
        return
    
    links = config["links"]
    
    m = kwargs["max ttl"]
    n = kwargs["number of queries"]
    
    for link in links.values():
        thread = Thread(target = traceroute_test, args = (link,m,n))
        thread.start()