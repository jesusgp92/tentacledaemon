import json
import logging

from settings import CONFIG_FILENAME

logging.getLogger()


def set_link(link):
    config = None
    try:
        config_file = open(CONFIG_FILENAME)
        config = json.loads(config_file.read())
        config["links"][str(link["id"])]=link
        config_file.close()
    except Exception as e:
        logging.exception("Config file can't be found or is corrupt for this monitor %s"%e)
        config = {"links":{}}
        config["links"][str(link["id"])]=link
    with open(CONFIG_FILENAME,'w+') as f:
        f.write(json.dumps(config))
        logging.info("config file updated successfully, added link %s"%link["id"])
def get_config():
    config = None
    try:
        config_file = open(CONFIG_FILENAME)
        config = json.loads(config_file.read())
    except Exception as e:
        logging.exception("Config file can't be found for this monitor %s"%e)
        config = {"links":{}}
    return config