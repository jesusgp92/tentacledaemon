import dropbox
import os

from settings import DATA_FOLDER,ACCESS_TOKEN,MONITOR_UNIQUE_IDENTIFIER
import datetime
import logging

logging.getLogger()

def upload_traces():
    
    upload_files = []
    client = dropbox.client.DropboxClient(ACCESS_TOKEN)
    now = datetime.datetime.now()
    current_hour = datetime.datetime(year=now.year,
                                     month=now.month,
                                     day=now.day,
                                     hour=now.hour)
    
    folders = os.listdir(DATA_FOLDER)
    for folder in folders:
        directory = os.path.join(DATA_FOLDER,folder)
        if not os.path.isdir(directory):
            continue
        files = os.listdir(directory)
        for trace_file in files:
            try:
                tokens = trace_file.split('_')
                trace_time = datetime.datetime(year=int(tokens[4])+2000,
                                  month=int(tokens[3]),
                                  day=int(tokens[2]),
                                  hour=int(tokens[5][:-4]))
            except ValueError as e:
                logging.error("Invalid filename found")
            if trace_time < current_hour:
                upload_files.append((trace_file,folder))
                 
    for trace_file,folder in upload_files:
        filename = os.path.join(DATA_FOLDER,folder,trace_file)
        f = open(filename, 'rb')
        dropbox_filename = '/monitor%s/%s/%s' % (MONITOR_UNIQUE_IDENTIFIER,folder,trace_file)
        try:
            response = client.put_file(dropbox_filename, f)
            f.close()
            os.remove(filename)
            logging.info("uploaded file to dropbox: %s"%response["path"])
        except dropbox.rest.ErrorResponse:
            logging.error("failed to upload file to dropbox: %s"%response["path"])