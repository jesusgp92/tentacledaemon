import imp
import logging
import os


PLUGIN_FOLDER = "plugins"
PATH = os.path.dirname(os.path.abspath(__file__))
MAIN_MODULE = "__init__"

logging.getLogger()

def get_plugins():
    plugins = {}
    try:
        candidates = os.listdir(os.path.join(PATH,PLUGIN_FOLDER))
        for candidate in candidates:
            location = os.path.join(PATH,PLUGIN_FOLDER, candidate)
            if not os.path.isdir(location) or not MAIN_MODULE + ".py" in os.listdir(location):
                continue
            info = imp.find_module(MAIN_MODULE, [location])
            plugins[candidate] = info
    except OSError as e:
        logging.ERROR(e)
    return plugins

def load_plugin(info):
    return imp.load_module(MAIN_MODULE, *info)