'''
Created on Oct 26, 2015

@author: root
'''

def get_file_type(path):
    filename = get_filename(path)
    if filename.startswith("test"):
        return "test"
    elif filename.startswith("link"):
        return "link"
    else:
        return "unknown"

def get_filename(path):
    pos = path.rfind("/") + 1
    return path[pos:]