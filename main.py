#!/usr/bin/env python
import sys
import logging
from logging.handlers import TimedRotatingFileHandler

from monitor.tentacle import TentacleDaemon
from logging import StreamHandler

LOG_FILENAME = "/tmp/monitor.log"
LOG_LEVEL = logging.DEBUG
LOG_FORMAT ='%(asctime)s %(levelname)-8s %(lineno)s %(name)s %(message)s'


if __name__ == "__main__":
    daemonize = True
    
    #handle options
    if len(sys.argv) >= 3:
        if sys.argv[2] == "--no-daemon":
            daemonize = False
            handler = StreamHandler(stream=sys.stdout)
            logging.basicConfig(format=LOG_FORMAT,
                        level=LOG_LEVEL,
                        handler=handler,
                        )
        else:
            print "unknown option:%s"%sys.argv[2]
    else:
        handler = TimedRotatingFileHandler(LOG_FILENAME,when="midnight",backupCount=3)
        logging.basicConfig(format=LOG_FORMAT,
                        filename= LOG_FILENAME,
                        level=LOG_LEVEL,
                        handler=handler,
                        )
    
    
    daemon = TentacleDaemon('/tmp/monitor.pid',daemonize)
    
    if len(sys.argv) >= 2:
        if 'start' == sys.argv[1]:
            daemon.start()
        elif 'stop' == sys.argv[1]:
            daemon.stop()
        elif 'restart' == sys.argv[1]:
            daemon.restart()
        elif 'jobs' == sys.argv[1]:
            daemon.print_jobs()
        else:
            print "Unknown command"
            sys.exit(2)
        sys.exit(0)
    else:
        print "usage: %s start|stop|restart|jobs [options]" % sys.argv[0]
        sys.exit(2)


